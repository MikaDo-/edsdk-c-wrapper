// stdafx.h�: fichier Include pour les fichiers Include syst�me standard,
// ou les fichiers Include sp�cifiques aux projets qui sont utilis�s fr�quemment,
// et sont rarement modifi�s
//

#pragma once

#include "targetver.h"

#include <cstdio>
#include <tchar.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <string>
#include <thread>

#include <vector>
#include <map>
#include <stack>
#include <queue>

#include <atlimage.h>

#include "EDSDK.h"

#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#define EDSWAPI __declspec(dllexport)

// TODO: faites r�f�rence ici aux en-t�tes suppl�mentaires n�cessaires au programme
