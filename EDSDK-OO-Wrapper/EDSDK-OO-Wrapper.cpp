// EDSDK-OO-Wrapper.cpp�: d�finit le point d'entr�e pour l'application console.
//
#include "stdafx.h"
#include <conio.h>

#include "DisplayableValue.h"
#include "Camera.h"
#include "CameraManager.h"
#include "EventManager.h"
#include "Task.h"

#define LV
//#define CAM2

using namespace EDSW;
using namespace std;

BOOL WINAPI CtrlHandler(DWORD ctrl);

int _tmain(int argc, _TCHAR* argv[])
{
	SetConsoleCtrlHandler(CtrlHandler, true); // Ajoute un handler pour le Ctrl+C

	EdsError err = EDS_ERR_OK;
	CameraManager::dumpCameraList("EOS.lst");
	CameraManager *cm = CameraManager::New();
    CameraPointer& cam = cm->getCamera(0);		// Pointeur *sympa* cf CameraManager.h

	cout << "Name 1: " << cam->getName() << endl << endl;
	cout << "Lens : " << cam->getPropString(kEdsPropID_LensName) << endl << endl;

    cam->setAv(AV_13_0);
    cam->setTv(TV_1_30);
	cam->setWb(WB_CLOUDY);
	cam->setISO(ISO_3200);
	cam->setPictureSize("S2");
	cam->setMeteringMode(MM_EVAL);
	cam->setSaveLocation(kEdsSaveTo_Host);

#ifdef LV
	if ( (err = cam->startLiveView()) )
		cout << "Erreur liveView on : 0x" << hex << err << endl;
	else
		cout << "LiveView On" << endl;
	char n[14] = "Canon EOS 60D";
	string fen(n);

	char k = ' ';
	while ((k = (char)cvWaitKey(20)) != 'q'){
		EdsGetEvent();
		if (k >= 'a' && k <= 'z'){
			switch (k){
				case 'f':
					err = cam->focus();
					cout << "focus " << hex << err << endl;
					break;
				case 'd':
					err = cam->takePicture();
					cout << "shoot " << hex << err << endl;
					break;
			}
		}
		if (!cam->isLiveViewOn())
			cam->startLiveView();
		cv::imshow(fen, Camera::downloadLiveView(0));
	}
	cv::destroyAllWindows();
	if (cam->isLiveViewOn())
		if ((err = cam->endLiveView()))
			cout << endl << "Erreur liveView off : 0x" << hex << err << endl;
		else
			cout << endl << "LiveView Off" << endl;
#endif
    delete cm;
	SetConsoleCtrlHandler(CtrlHandler, false); // retire le handler pour le Ctrl+C
	_getch();
    return EXIT_SUCCESS;
}

BOOL WINAPI CtrlHandler(DWORD ctrl){
	if (ctrl == CTRL_C_EVENT || ctrl == CTRL_BREAK_EVENT || ctrl == CTRL_CLOSE_EVENT || ctrl == CTRL_LOGOFF_EVENT || ctrl == CTRL_SHUTDOWN_EVENT){
		delete CameraManager::New();
		ExitProcess(1);
	}
	return true;
}
  