#include "stdafx.h"
#include "DisplayableValue.h"
#include "Camera.h"
#include "Task.h"
#include "FileTransferTask.h"

using namespace std;
using namespace EDSW;

FileTransferTask::FileTransferTask(Camera *cam, const TaskID &taskID, ...) : Task(cam), m_taskID(taskID), m_progress(0)
{
    va_list args;
    va_start(args, taskID);
    this->run(args);
    va_end(args);
}

void FileTransferTask::run(va_list &args){
    m_status = WORKING;
    switch (m_taskID){
        case DL_LAST_PIC:{
            thread t(Camera::downloadLastShot, m_cam, this);
            m_statusDisp = "Retrieving last shot from camera...";
            break;
        }
        case DATA_COPY:{
            EdsStreamRef from = va_arg(args, EdsStreamRef);
            EdsUInt32    size = va_arg(args, EdsUInt32);
            EdsStreamRef to = va_arg(args, EdsStreamRef);
            cout << "size : " << size << endl;
            thread t(Camera::copyStream, m_cam, this, from, size, to);
            EdsSetProgressCallback(from, FileTransferTask::progressCallback, kEdsProgressOption_Periodically, this);
            m_statusDisp = "Copying data...";
            break;
        }
        default:
            cout << "Camera [" << m_cam->getId() << "] ERROR : TaskID " << m_taskID << " not supported." << endl;
            break;
    }
}

EdsError EDSCALLBACK FileTransferTask::progressCallback(EdsUInt32 progress, EdsVoid* taskPointer, EdsBool* cancel){
    if (!*cancel){
        FileTransferTask* task = (FileTransferTask*)taskPointer;
        task->m_progress = progress;
        cout << ".";
        if (task->m_status == CANCELING){
            *cancel = true;
            task->m_status = FINISHED_ABORTED;
            task->m_statusDisp = "Canceled";
            cout << "operation " << task->m_taskID << " for camera [" << task->m_cam->getId() << "] canceled" << endl;
        }
    }
    return EDS_ERR_OK;
}


FileTransferTask::~FileTransferTask()
{
}

void FileTransferTask::cancel(){
    m_status = CANCELING;
}
int FileTransferTask::getProgress() const{
    return m_progress;
}
bool FileTransferTask::isCanceled() const{
    return (m_status == CANCELING || m_status == FINISHED_ABORTED);
}
TaskID FileTransferTask::getTaskID() const{
    return m_taskID;
}