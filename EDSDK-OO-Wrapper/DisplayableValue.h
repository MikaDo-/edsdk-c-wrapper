#pragma once

using namespace std;

namespace EDSW{
    template<typename T>
    class DisplayableValue
    {
        protected:
            T m_val;
            string m_disp;
        public:
            DisplayableValue(const T &val, const string &disp): m_val(val), m_disp(disp){}
            ~DisplayableValue();

            string getString() const{ return m_disp;                }
            char*  getText()   const{ return (char*)m_disp.c_str(); }
            T      getValue()  const{ return m_val;                 }
    };
};

