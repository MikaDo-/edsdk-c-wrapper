#pragma once

namespace EDSW{
    class FileTransferTask : public Task
    {
        protected:
            TaskID        m_taskID;
            unsigned int  m_progress;     // 0 - 100

        public:
            FileTransferTask(Camera *cam, const TaskID &taskid, ...);
            virtual ~FileTransferTask();


            void          run(va_list &args);
            int           getProgress() const;
            void          cancel();
            bool          isCanceled() const;
            TaskID        getTaskID() const;

            // progress callback ici
            static EdsError EDSCALLBACK progressCallback(EdsUInt32 progress, EdsVoid* taskPointer, EdsBool* cancel);
    };
};
