#pragma once

using namespace std;

typedef std::function<EdsError(EdsPropertyEvent, EdsPropertyID, EdsUInt32, EdsVoid*)>   EdswPropertyEventHandler;
typedef std::function<EdsError(EdsStateEvent,    EdsUInt32,     EdsVoid*)>              EdswStateEventHandler;
typedef std::function<EdsError(EdsObjectEvent,   EdsBaseRef,    EdsVoid*)>              EdswObjectEventHandler;

namespace EDSW{
    class Task;
    class PropertyTask;
	class EventManager;
    class CameraManager;
    typedef enum{
		DATA_COPY, DL_LAST_PIC, DL_PIC
    } TaskID;

    typedef struct{
        IplImage*	  IplFrame;  // Strictement réservé au liveview !!!! (si on ouvre un 1920*180, qu'on écrit ensuite du liveview dedans, le résultat n'est pas... pas celui recherché en tous cas...)
        EdsStreamRef  stream;
        cv::Mat		  frame;
        EdsImageInfo  imageInfo;
        bool		  flip;
        EdsEvfImageRef evfRef;
    } LiveViewDataSet;

    class Camera
    {
		friend EventManager;
        protected:
			// Base
            int m_id;													// Identifiant unique aurpès du CameraManager
            string m_name;												// Modèle de l'appareil
            CameraManager* m_cm;										// CameraManager parent
			// Configuration
			string m_pictureFilename;									// Nom de la prochaine photo enregistrée
            EdsCameraRef m_edsReference;								// Référence du SDK Canon
			// Variables d'état
            bool m_busy;												// Inutilisé
			bool m_bulb;												// Inutilisé
			bool m_online;												// L'appareil est-il connecté ?
			bool m_liveView;											// L'appareil est-il en live view ?
            bool m_uiLocked;                                            // Les contrôles physiques sont-ils bloqués ?
			bool m_saveOntoSD;											// Sauvegarde-t-on sur la carte SD ?
            bool m_sessionOpened;										// La session Edsdk est-elle ouverte pour cet appareil ?
			bool m_pictureDownloading;									// Y a-t-il une photo en train de se télécharger pour cet appareil ?
            EdsDirectoryItemRef m_lastShot;								// Inutilisé
			// Available settings
            vector<int>   m_availableISO;								// Sensibilités ISO disponilbes pour l'appareil
            vector<float> m_availableAV;								// Ouvertures disponibles avec l'objectif courant
            vector<float> m_availableTV;								// Temps d'exposition disponibles
            vector<int>   m_availableWB;								// Balances des blancs disponibles
            vector<int>   m_availableAE;								// Modes AE disponibles (Auto, P, Tv, Av, M, CA, ...)
            vector<int>   m_availableMeteringModes;						// Modes de mesure d'expositions disponibles
			// Tables de conversion hexa EDSDK => Valeur affichable
            static map<int, DisplayableValue<int>*>   s_ISO;			// table conversion valeurs ISO
            static map<int, DisplayableValue<float>*> s_AV;				// table conversion valeurs Av
            static map<int, DisplayableValue<float>*> s_TV;				// table conversion valeurs Tv
            static map<int, DisplayableValue<int>*>   s_WB;				// table conversion valeurs balance des blancs
            static map<int, DisplayableValue<int>*>   s_AE;				// table conversion valeurs de modes AE
            static map<int, DisplayableValue<int>*>   s_meteringModes;	// table conversion valeurs de mesure d'exposition
			static map<string, EdsUInt32>			  s_pictureSizes;	// tailles de photos

			// Tâches (INUTILISÉ)
			// TODO :TaskRunner *m_taskRunner;
            queue<Task*> m_tasks; // pointeurs pour le destructeur virtuel des classes filles filetransfer,...
            // ---

			// Gestion des événements 
            stack<EdswStateEventHandler>	m_stateEventHandlers;
            stack<EdswObjectEventHandler>	m_objectEventHandlers;
            stack<EdswPropertyEventHandler>	m_propertyEventHandlers;
            // ---

            LiveViewDataSet                 m_lvDataSet;

            static bool s_debug;
        public:

            Camera();
            Camera(const int &id, const EdsCameraRef &camRef, CameraManager* cm);
            virtual ~Camera();

            EdsCameraRef getEdsReference() const;
            unsigned int getId() const;
            string getName() const;
			CameraManager* getCameraManager();

            EdsError openSession();
            EdsError closeSession();
            EdsError askAvaibleSettings();

			EdsError setPictureSize(const string &size);

			EdsError startBulb();
			EdsError endBulb();

            EdsError lockUI(const bool &force = false);
            EdsError unlockUI(const bool &force = false);
            EdsError setUiLocked(const bool &locked, const bool &force = false);
            EdsBool  isUiLocked() const;

			EdsError takePicture(const string& filename = "", const bool &forceAF = false);
			EdsError setSaveLocation(const EdsUInt32 &newSaveLocation);

            EdsError getAvRaw(EdsUInt32 *av) const;
            EdsError getAv(EdsFloat *av) const;
            string   getAvString() const;
            EdsError setAv(EdsUInt32 av) const;

            EdsError getTvRaw(EdsUInt32 *tv) const;
            EdsError getTv(EdsFloat *tv) const;
            string   getTvString() const;
            EdsError setTv(EdsUInt32 tv) const;

            EdsError getAEModeRaw(EdsUInt32 *aeMode) const;
            EdsError getAEMode(EdsUInt32 *aeMode) const;
            string   getAEModeString() const;
            EdsError setAEMode(EdsUInt32  aeMode) const;

            EdsError getWbRaw(EdsUInt32 *wb) const;
            EdsError getWb(EdsUInt32 *wb) const;
            string   getWbString() const;
            EdsError setWb(EdsUInt32 wb) const;

            EdsError getISORaw(EdsUInt32 *iso) const;
            EdsError getISO(EdsUInt32 *iso) const;
            string   getISOString() const;
            EdsError setISO(EdsUInt32 iso) const;

            EdsError getMeteringModeRaw(EdsUInt32 *mm) const;
            EdsError getMeteringMode(EdsUInt32 *mm) const;
            string   getMeteringModeString() const;
            EdsError setMeteringMode(EdsUInt32 mm) const;

			EdsError getProp(const EdsPropertyID &propId, EdsVoid *propVal) const;
            string   getPropString(const EdsPropertyID &propId) const;
            EdsError setProp(const EdsPropertyID &propId, EdsVoid *propVal) const;
			EdsError setPropByVal(const EdsPropertyID &propId, const EdsUInt32 &propVal) const;

            // callbacks actuelles (pointeurs sur les têtes des piles du dessus)
			EdswStateEventHandler*      stateEventHandler;
			EdswObjectEventHandler*     objectEventHandler;
			EdswPropertyEventHandler*   propertyEventHandler;

			EdsError pushStateEventHandler(const EdswStateEventHandler& func);
			EdsError pushObjectEventHandler(const EdswObjectEventHandler& func);
			EdsError pushPropertyEventHandler(const EdswPropertyEventHandler& func);
			EdsError popStateEventHandler();
			EdsError popObjectEventHandler();
            EdsError popPropertyEventHandler();

			EdsError pushTask(Task *t);
			EdsError popTask();

			void setPictureFilename(const string &filename);
			string getPictureFilename() const;
			EdsError downloadImage(const EdsDirectoryItemRef &directoryItem);
			bool isPictureDownloading() const;
            static void copyStream(Camera *cam, Task *t, EdsStreamRef from, EdsUInt32 size, EdsStreamRef to);

            EdsError focus();
			EdsBool  isLiveViewOn() const;
            void     setLiveView(const bool &lv);
			void	 setLvFlip(const bool &);

			EdsBool  isOnline() const;
			void	 setOnline(const bool &online);

            EdsError startLiveView();
			EdsError endLiveView();
			EdsError allocLiveViewFrame();
			EdsError updateLiveView();
			cv::Mat  downloadLiveView();
			static EdsError updateLiveView(const unsigned int &camID);
			static cv::Mat  downloadLiveView(const unsigned int &camID);
            LiveViewDataSet* getLvDataSet();

			IplImage* getLvIplFrame();
			cv::Mat& getlvFrame();

            friend ostream& operator<<(ostream &o, const Camera &);
    };
};

//!\\ Scroller davantage ne résultera qu'en tristesse et désepsoir. J'ai prévenu.

#define ISO_6	       0x28
#define ISO_12	       0x30
#define ISO_25	       0x38
#define ISO_50	       0x40
#define ISO_100        0x48
#define ISO_125	       0x4B
#define ISO_150	       0x4D
#define ISO_200        0x50
#define ISO_250	       0x53
#define ISO_320	       0x55
#define ISO_400        0x58
#define ISO_500	       0x5B
#define ISO_640	       0x5D
#define ISO_800        0x60
#define ISO_1000       0x63
#define ISO_1250       0x65
#define ISO_1600       0x68
#define ISO_3200       0x70
#define ISO_6400       0x78
#define ISO_12800      0x80
#define ISO_25600      0x88
#define ISO_51200      0x90
#define ISO_102400     0x98

#define AV_1_0         0x08
#define AV_1_1         0x0B
#define AV_1_2         0x0C
#define AV_1_4         0x10
#define AV_1_6         0x13
#define AV_1_8         0x14
#define AV_2_0         0x18
#define AV_2_2         0x1B
#define AV_2_5         0x1C
#define AV_2_8         0x20
#define AV_3_2         0x23
#define AV_3_5         0x24
#define AV_4_0         0x28
#define AV_4_5         0x2B
#define AV_5_0         0x2D
#define AV_5_6         0x30
#define AV_6_3         0x33
#define AV_7_1         0x35
#define AV_8_0         0x38
#define AV_9_0         0x3B
#define AV_9_5         0x3C
#define AV_10_0        0x3D
#define AV_11_0        0x40
#define AV_13_0        0x44
#define AV_14_0        0x45
#define AV_16_0        0x48
#define AV_18_0        0x4B
#define AV_19_0        0x4C
#define AV_20_0        0x50
#define AV_22_0        0x50
#define AV_25_0        0x53
#define AV_27_0        0x54
#define AV_29_0        0x55
#define AV_32_0        0x58
#define AV_36_0        0x5B
#define AV_38_0        0x5C
#define AV_40_0        0x5D
#define AV_45_0        0x60
#define AV_51_0        0x63
#define AV_54_0        0x64
#define AV_57_0        0x66
#define AV_64_0        0x68
#define AV_72_0        0x6B
#define AV_76_0        0x6C
#define AV_80_0        0x6D
#define AV_91_0        0x70

#define TV_BULB        0x0C
#define TV_30s         0x10
#define TV_25s         0x13
#define TV_20s         0x14
#define TV_15s         0x18
#define TV_13s         0x1B
#define TV_10s         0x1C
#define TV_8s          0x20
#define TV_6s          0x24
#define TV_5s          0x25
#define TV_4s          0x28
#define TV_3_2s        0x2B
#define TV_3s          0x2C
#define TV_2_5s        0x2D
#define TV_2s          0x30
#define TV_1_6s        0x33
#define TV_1_5s        0x34
#define TV_1_3s        0x35
#define TV_1s          0x38
#define TV_0_8s        0x3B
#define TV_0_7s        0x3C
#define TV_0_6s        0x3D
#define TV_0_5s        0x40
#define TV_0_4s        0x43
#define TV_0_3s        0x45
#define TV_1_4         0x48
#define TV_1_5         0x4B
#define TV_1_6         0x4C
#define TV_1_8         0x50
#define TV_1_10        0x54
#define TV_1_13        0x55
#define TV_1_15        0x58
#define TV_1_20        0x5C
#define TV_1_25        0x5D
#define TV_1_30        0x60
#define TV_1_40        0x63
#define TV_1_45        0x64
#define TV_1_50        0x65
#define TV_1_60        0x68
#define TV_1_80        0x6B
#define TV_1_90        0x6C
#define TV_1_100       0x6D
#define TV_1_125       0x70
#define TV_1_160       0x73
#define TV_1_180       0x74
#define TV_1_200       0x75
#define TV_1_250       0x78
#define TV_1_320       0x7B
#define TV_1_350       0x7C
#define TV_1_400       0x7D
#define TV_1_500       0x80
#define TV_1_640       0x83
#define TV_1_750       0x84
#define TV_1_800       0x85
#define TV_1_1000      0x88
#define TV_1_1250      0x8B
#define TV_1_1500      0x8C
#define TV_1_1600      0x8D
#define TV_1_2000      0x90
#define TV_1_2500      0x93
#define TV_1_3000      0x94
#define TV_1_3200      0x95
#define TV_1_4000      0x98
#define TV_1_5000      0x9B
#define TV_1_6000      0x9C
#define TV_1_6400      0x9D
#define TV_1_8000      0xA0

#define MM_SPOT        0x01
#define MM_EVAL        0x03
#define MM_PARTIAL     0x04
#define MM_CENTER      0x05

#define WB_AUTO	       0x00
#define WB_DAYLIGHT	   0x01
#define WB_CLOUDY	   0x02
#define WB_TUNGSTEN	   0x03
#define WB_FLUORESCENT 0x04
#define WB_FLASH	   0x05
#define WB_MANUAL1	   0x06
#define WB_MANUAL2	   0x0F
#define WB_MANUAL3	   0x11
#define WB_MANUAL4	   0x12
#define WB_MANUAL5	   0x13
#define WB_SHADE	   0x07
#define WB_COLORTEMP   0x09
#define WB_PC1		   0x0A
#define WB_PC2	       0x0B
#define WB_PC3	       0x0C
#define WB_PC4	       0x14
#define WB_PC5	       0x15

#define AF_ONE_SHOT	   0x0
#define AF_AI_SERVO	   0x1
#define AF_AI_FOCUS	   0x2
#define AF_MANUAL	   0x3
