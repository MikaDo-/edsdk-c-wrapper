#pragma once

namespace EDSW{
    typedef enum{
        STOPPED = 0,
        WORKING = 1,
        CANCELING = 2,
        FINISHED_SUCCESS = 3,
        FINISHED_FAILURE = 4,
        FINISHED_ABORTED = 5
    } TaskStatus;
    class Task
    {
        friend class Camera;

        protected:
            Camera*       m_cam;
            TaskStatus    m_status;
            string        m_statusDisp;
            EdsError      m_error;
            void*         m_result;

        public:
            Task(Camera *cam);
            ~Task();

            bool          isFinished() const;
            bool          isStarted() const;
            bool          isSuccessful() const;
            TaskStatus    getStatus() const;
            string        getStatusString() const;
            void*         getResult();
            void          setError(const EdsError &err);
            EdsError      getError() const;
    };
};
