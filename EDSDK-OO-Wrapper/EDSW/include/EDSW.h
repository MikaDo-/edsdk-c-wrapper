#pragma once

#include <cstdio>
#include <tchar.h>
#include <strsafe.h>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <string>
#include <thread>

#include <vector>
#include <map>
#include <stack>
#include <queue>

#include <atlimage.h>
#include "EDSDK.h"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"

#include "DisplayableValue.h"
#include "Camera.h"
#include "CameraManager.h"
#include "EventManager.h"
#include "Task.h"
#include "TaskSequence.h"