#pragma once

using namespace std;

namespace EDSW{
	// Cette structure permet d'avoir un pointeur vers une cam�ra pouvant �tre connect�e/d�conect�e.
	// L'adresse de l'objet Camera peut chager. Auquel cas le CameraPointer sera mis � jour par CameraManager
	typedef struct CameraPointer_struct{
		Camera* pointer;
		CameraPointer_struct() : pointer(NULL){}
		CameraPointer_struct(Camera* cam) : pointer(cam){}
		Camera* operator->(){
			return this->pointer;
		}
	} CameraPointer;

    class CameraManager
    {
        protected:
            vector<Camera*> m_cameras;

			static bool s_isInstantiated;
			static CameraManager* s_instance;
            CameraManager();

			vector<CameraPointer> m_cameraPointers; // � chaque cam�ra son pointeur

        public:
			static CameraManager* New();
            static bool isInstantiated();
            virtual ~CameraManager();

            int refreshCameraList();
			static EdsError dumpCameraList(char *path);
            CameraPointer&  getCamera(const unsigned int &index);
            unsigned int    getCameraCount() const;

            void openSessions();
            void closeSessions();

            Camera* operator[](const int &i);
            //friend ostream& operator<<(ostream &o, CameraManager &cm);
    };
};
