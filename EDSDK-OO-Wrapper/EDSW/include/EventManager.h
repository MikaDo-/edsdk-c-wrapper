#pragma once

#define EDSW_EVENT_NOT_CAUGHT	0x00000010L

namespace EDSW{
    class EventManager
    {
        protected:
            CameraManager* m_cm;

            static EventManager* s_instance;
            static bool s_isInstantiated;
        public:
            EventManager(CameraManager* cm);
            ~EventManager();

            static EventManager* New(CameraManager* cm = NULL);

            void setupCamera(Camera *cam); // sets up the callbacks for the specified Camera

            // Callbacks appel�es par EDSDK
            static EdsError EDSCALLBACK EdsCameraAddedHandler  (EdsVoid* context);
            static EdsError EDSCALLBACK EdsObjectEventHandler  (EdsObjectEvent   event, EdsBaseRef    ref, EdsVoid*  context);
            static EdsError EDSCALLBACK EdsPropertyEventHandler(EdsPropertyEvent event, EdsPropertyID propID, EdsUInt32 param, EdsVoid* context);
            static EdsError EDSCALLBACK EdsStateEventHandler   (EdsStateEvent    event, EdsUInt32     eventData, EdsVoid*  context);
    };
};
