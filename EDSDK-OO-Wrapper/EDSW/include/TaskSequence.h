#pragma once

namespace EDSW{
	class TaskSequence : public Task
	{
		protected:
			vector<std::pair<Task*, bool>> m_tasks;
			std::thread *m_thread;
			bool m_canceled;
			unsigned int m_progress;

		public:
			TaskSequence(Camera *cam, const TaskID &taskID, ...);
			~TaskSequence();

			void pushTask(Task* t);
			bool popTask();

			unsigned int size();
			void clear();
			bool start();
			void run();

			unsigned int getProgress() const;
	};
};
