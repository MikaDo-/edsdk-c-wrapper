#include "stdafx.h"

#include "DisplayableValue.h"
#include "Camera.h"
#include "Task.h"

using namespace std;
using namespace EDSW;


Task::Task(Camera *cam) : m_cam(cam), m_status(STOPPED), m_statusDisp("Initializing"), m_result(NULL)
{
    cout << "New task instanciated for camera " << cam->getId() << endl;
}

Task::~Task()
{
}

bool Task::isFinished() const{
    return (m_status >2);
}
bool Task::isStarted() const{
    return m_status != STOPPED;
}
bool Task::isSuccessful() const{
    return m_status == FINISHED_SUCCESS;
}
TaskStatus Task::getStatus() const{
    return m_status;
}
string Task::getStatusString() const{
    return m_statusDisp;
}
void* Task::getResult(){
    return m_result;
}
void Task::setError(const EdsError &err){
    m_error = err;
}
EdsError Task::getError() const{
    return m_error;
}