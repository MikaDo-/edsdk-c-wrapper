#include "stdafx.h"

#include "DisplayableValue.h"
#include "Camera.h"
#include "Task.h"
#include "CameraManager.h"
#include "EventManager.h"
#include "jpeglib.h"
#include "turbojpeg.h"

using namespace EDSW;
using namespace std;


Camera::Camera(){

}
//!\ Le constructeur est execut� AVANT EdsOpenSession
Camera::Camera(const int &id, const EdsCameraRef &camRef, CameraManager *cm): m_id(id), m_edsReference(camRef), m_sessionOpened(false), m_cm(cm){
    EdsError err;
    EdsDeviceInfo dev;
	m_lvDataSet.flip = true;
	m_liveView = false;
    m_lvDataSet.stream = NULL;
    m_lvDataSet.IplFrame = NULL;
    m_lvDataSet.evfRef = NULL;
    m_lvDataSet.frame = cv::cvarrToMat(cvLoadImage("nocam1.jpg"));
	m_pictureFilename = "";
	m_pictureDownloading = false;
	
    if( !(err = EdsGetDeviceInfo(camRef, &dev)) )
        m_name = string(dev.szDeviceDescription);
    else
        cout << "Erreur obtention du nom pour appareil ["<< id <<"]: 0x" << hex << err << endl;
    
    // d�finition des callbacks par d�faut.
	pushPropertyEventHandler(
		EdswPropertyEventHandler(
		[](EdsPropertyEvent a, EdsPropertyID b, EdsUInt32 c, EdsVoid* d) -> EdsError { cout << "default PropertyEventHandler. PropID:" << hex << b << endl; return EDS_ERR_OK; }
		)
	);
	pushObjectEventHandler(
		EdswObjectEventHandler(
			[](EdsPropertyEvent event, EdsBaseRef ref, EdsVoid* context) -> EdsError {
				EdsError err = EDS_ERR_OK;
				Camera *cam = (Camera*)context;
				cout << "================================================================================================> EventManager : objectEvent" << endl;
				cout << "default ObjecEventHandler. Event : 0x" << hex << event << " - Ref: 0x" << hex << ref << endl;

				if (event == kEdsObjectEvent_DirItemRequestTransfer || event == kEdsObjectEvent_DirItemRequestTransferDT){
					cout << "OK @EventManager::EdsObjectEventHandler - ref = 0x" << hex << ref << endl;
					if ((err = cam->downloadImage(ref)))
						cout << "ERROR @defaultObjectEventHandler:" << __LINE__ << " Could not download image - 0x" << hex << err << endl;
					return err;
				}else if (event == kEdsObjectEvent_DirItemCreated){
					if ((err = cam->downloadImage(ref))){
						cout << "ERROR @defaultObjectEventHandler:" << __LINE__ << " Could not download image - 0x" << hex << err << endl;
						return err;
					}
					if ((err = EdsDeleteDirectoryItem(ref)) )
						cout << "ERROR @defaultObjectEventHandler:" << __LINE__ << " Could not delete remote image - 0x" << hex << err << endl;
					return err;
				}else
					return err;
			}
		)
	);
    pushStateEventHandler(
        EdswStateEventHandler(
            [](EdsStateEvent event, EdsUInt32 param, EdsVoid* context) -> EdsError {
				Camera *cam = (Camera*)context;
				cout << ">>>>>>>>>>>>>> default StateEventHandler. stateEvent:" << hex << event << ", param: " << hex << param << endl;
				if (event == kEdsStateEvent_Shutdown){
					cam->endLiveView();
					cam->setOnline(false);
					return EDS_ERR_OK;
				}
				else if (event == kEdsStateEvent_WillSoonShutDown){
					return EdsSendCommand(cam->getEdsReference(), kEdsCameraCommand_ExtendShutDownTimer, 0);
				}else
					return EDS_ERR_OK;
			}
        )
    );
}

Camera::~Camera(){
	if (m_liveView)
		endLiveView();
	// TODO : release de tout le bordel
    this->closeSession();
}

EdsError Camera::openSession(){
    EdsError err;
    cout << "Opening session for device [" << m_id << "]";
    if (!(err = EdsOpenSession(m_edsReference))){
        cout << "\t\t\t\t [OK]" << endl;
		m_sessionOpened = true;
		m_online = true;
    }else
        cout << "\t\t\t\t [Error: 0x" << hex << err << "]" << endl;

	EdsUInt32 af;
	af = Evf_AFMode_Live;
	setProp(kEdsPropID_Evf_AFMode, &af);
	setSaveLocation(kEdsSaveTo_Host);
    setUiLocked(false, true);
    return err;
}
EdsError Camera::closeSession(){
    EdsError err;
    cout << "Closing session for device [" << m_id << "]";
    if (!(err = EdsCloseSession(m_edsReference))){
        cout << "\t\t\t\t [OK]" << endl;
        m_sessionOpened = false;
    }else
        cout << "\t\t\t\t [Error: 0x" << hex << err << "]" << endl;
    return err;
}

EdsBool Camera::isOnline() const{
	return m_online;
}

void Camera::setOnline(const bool &online){
	m_online = online;
}

// � corriger
EdsError Camera::askAvaibleSettings(){
    EdsPropertyDesc propDesc;
    EdsPropertyID   m_propIDsI[]   = { kEdsPropID_ISOSpeed,  kEdsPropID_WhiteBalance,  kEdsPropID_AEMode,         kEdsPropID_MeteringMode };
    EdsPropertyID   m_propIDsF[]   = { kEdsPropID_Av,        kEdsPropID_Av };
    vector<int>*    m_propDescsI[] = { &m_availableISO,      &m_availableAE,           &m_availableMeteringModes };
    vector<float>*  m_propDescsF[] = { &m_availableAV,       &m_availableTV };  
    map<int, DisplayableValue<int>*>*   m_propConvertI[] = { &s_ISO,                   &s_AE,                     &s_meteringModes };
    map<int, DisplayableValue<float>*>* m_propConvertF[] = { &s_AV,                    &s_TV };

    for (int j(0); j < 4; j++){
        EdsGetPropertyDesc(m_edsReference, m_propIDsI[j], &propDesc);
        for (int i(0); i < propDesc.numElements; i++)
            m_propDescsI[j]->push_back( (*(m_propConvertI[j]))[propDesc.propDesc[i]]->getValue() );
    }
    for (int j(0); j < 4; j++){
        EdsGetPropertyDesc(m_edsReference, m_propIDsF[j], &propDesc);
        for (int i(0); i < propDesc.numElements; i++)
            m_propDescsF[j]->push_back( (*(m_propConvertF[j]))[propDesc.propDesc[i]]->getValue() );
    }
    return EDS_ERR_OK;
}

EdsError Camera::lockUI(const bool &force){
    EdsError err = EDS_ERR_OK;
    if (m_uiLocked && !force)
        return err;
    if ((err = EdsSendStatusCommand(m_edsReference, kEdsCameraStatusCommand_UILock, 0)))
        cout << "ERROR @Camera::lockUI - Cannot lock UI - 0x" << hex << err << endl;
    else
        m_uiLocked = true;
    return err;
}
EdsError Camera::unlockUI(const bool &force){
    EdsError err = EDS_ERR_OK;
    if (!m_uiLocked && !force)
        return err;
    if ((err = EdsSendStatusCommand(m_edsReference, kEdsCameraStatusCommand_UILock, 0)))
        cout << "ERROR @Camera::lockUI - Cannot lock UI - 0x" << hex << err << endl;
    else
        m_uiLocked = false;
    return err;
}
EdsError Camera::setUiLocked(const bool &locked, const bool &force){
    if (locked)
        return lockUI(force);
    return unlockUI(force);
}
EdsBool  Camera::isUiLocked() const{
    return m_uiLocked;
}

/* *************************************************************************************** */
/* **************************  A P E R T U R E   V A L U E  ****************************** */
/* *************************************************************************************** */

EdsError Camera::getAvRaw(EdsUInt32 *av) const{
    EdsError err;
    err = EdsGetPropertyData(m_edsReference, kEdsPropID_Av, 0, sizeof(EdsUInt32), av);
    if (err && Camera::s_debug)
        cout << "ERREUR get propVal Tv : 0x" << hex << err << endl;
    return err;
}
EdsError Camera::getAv(EdsFloat *av) const{
    EdsError err;
    EdsUInt32 avr;

    err = getAvRaw(&avr);
    if (!err)
        *av = Camera::s_AV[avr]->getValue();
    return err;
}
string Camera::getAvString() const{
    EdsUInt32 avr;

    if (getAvRaw(&avr))
        return string("ERREUR");
    return Camera::s_AV[avr]->getString();
}
EdsError Camera::setAv(EdsUInt32 av) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_Av, 0, sizeof(EdsUInt32), &av);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR set propVal Av : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* *********************************  T I M E   V A L U E  ******************************* */
/* *************************************************************************************** */

EdsError Camera::getTvRaw(EdsUInt32 *tv) const{
    EdsError err;
    err = EdsGetPropertyData(m_edsReference, kEdsPropID_Tv, 0, sizeof(EdsUInt32), tv);
    if (err && Camera::s_debug)
        cout << "ERREUR get propVal Tv : 0x" << hex << err << endl;
    return err;
}

EdsError Camera::getTv(EdsFloat *tv) const{
    EdsError err;
    EdsUInt32 tvr;
    err = getTvRaw(&tvr);
    if (!err)
        *tv = Camera::s_TV[tvr]->getValue();
    return err;
}
string Camera::getTvString() const{
    EdsUInt32 tvr;

    if (getTvRaw(&tvr))
        return string("ERREUR");
    return Camera::s_TV[tvr]->getString();
}
EdsError Camera::setTv(EdsUInt32 tv) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_Tv, 0, sizeof(EdsUInt32), &tv);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR set propVal Tv : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* ************************************  A E   M O D E  ********************************** */
/* *************************************************************************************** */

EdsError Camera::getAEModeRaw(EdsUInt32 *aeMode) const{
    EdsError err;
    err = EdsGetPropertyData(m_edsReference, kEdsPropID_AEMode, 0, sizeof(EdsUInt32), aeMode);
    if (err && Camera::s_debug)
        cout << "ERREUR get propVal AE : 0x" << hex << err << endl;
    return err;
}
EdsError Camera::getAEMode(EdsUInt32 *aeMode) const{
    EdsError err = getAEModeRaw(aeMode); // un peu inutile pour l'instant. Sera utile lorse que corres id<->val
    return err;
}
string Camera::getAEModeString() const{
    EdsUInt32 aer;

    if (getAEModeRaw(&aer))
        return string("ERREUR");
    return Camera::s_AE[aer]->getString();
}
EdsError Camera::setAEMode(EdsUInt32 aeMode) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_AEMode, 0, sizeof(EdsUInt32), &aeMode);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR set propVal AE : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* *****************************   W H I T E   B A L A N C E  **************************** */
/* *************************************************************************************** */

EdsError Camera::getWbRaw(EdsUInt32 *wb) const{
    EdsError err;
    err = EdsGetPropertyData(m_edsReference, kEdsPropID_WhiteBalance, 0, sizeof(EdsUInt32), wb);
    if (err && Camera::s_debug)
        cout << "ERREUR propVal WB : 0x" << hex << err << endl;
    return err;
}
EdsError Camera::getWb(EdsUInt32 *wb) const{
    EdsError err;
    EdsUInt32 wbr;
    err = getWbRaw(&wbr);
    if (!err)
        *wb = Camera::s_WB[*wb]->getValue();
    return err;
}
string Camera::getWbString() const{
    EdsUInt32 wbr;

    if (getWbRaw(&wbr))
        return string("ERREUR");
    return Camera::s_WB[wbr]->getString();
}
EdsError Camera::setWb(EdsUInt32 wb) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_WhiteBalance, 0, sizeof(EdsUInt32), &wb);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR propVal WB : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* *********************************   I S O   S P E E D  ******************************** */
/* *************************************************************************************** */

EdsError Camera::getISORaw(EdsUInt32 *iso) const{
    return EdsGetPropertyData(m_edsReference, kEdsPropID_ISOSpeed, 0, sizeof(EdsUInt32), iso);
}
EdsError Camera::getISO(EdsUInt32 *iso) const{
    EdsError err;
    EdsUInt32 isor;

    err = getISORaw(&isor);
    if (!err)
        *iso = Camera::s_ISO[isor]->getValue();
    return err;
}
string Camera::getISOString() const{
    EdsUInt32 isor;

    if (this->getISORaw(&isor))
        return string("ERREUR");
    return Camera::s_ISO[isor]->getString();
}
EdsError Camera::setISO(EdsUInt32 iso) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_ISOSpeed, 0, sizeof(EdsUInt32), &iso);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR set propVal : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* ***************************  M E T E R I N G   M O D E S  ***************************** */
/* *************************************************************************************** */

EdsError Camera::getMeteringModeRaw(EdsUInt32 *iso) const{
    return EdsGetPropertyData(m_edsReference, kEdsPropID_MeteringMode, 0, sizeof(EdsUInt32), iso);
}
EdsError Camera::getMeteringMode(EdsUInt32 *mm) const{
    EdsError err;
    EdsUInt32 mmr;

    err = getMeteringModeRaw(&mmr);
    if (!err)
        *mm = Camera::s_meteringModes[mmr]->getValue();
    return err;
}
string Camera::getMeteringModeString() const{
    EdsUInt32 mmr;

    if (this->getMeteringModeRaw(&mmr))
        return string("ERREUR");
    return Camera::s_meteringModes[mmr]->getString();
}
EdsError Camera::setMeteringMode(EdsUInt32 mm) const{
    EdsError err;

    err = EdsSetPropertyData(m_edsReference, kEdsPropID_MeteringMode, 0, sizeof(EdsUInt32), &mm);
    Sleep(200);
    EdsGetEvent();
    if (err && Camera::s_debug)
        cout << "ERREUR set propVal : 0x" << hex << err << endl;
    return err;
}

/* *************************************************************************************** */
/* ***************************   G E N E R I C   P R O P E R T Y  ************************ */
/* *************************************************************************************** */

EdsError Camera::getProp(const EdsPropertyID &propId, EdsVoid *propVal) const{
    EdsError err;
    EdsDataType dt;
    EdsUInt32 ds;

    err = EdsGetPropertySize(m_edsReference, propId, 0, &dt, &ds);
    if(!err){

        err =EdsGetPropertyData(m_edsReference, propId, 0, ds, propVal);
        if (err && Camera::s_debug)
            cout << "ERREUR get propVal generique '" << propId << "'. Code 0x" << hex << err << endl;
    }
    else if (err && Camera::s_debug)
        cout << "ERREUR get propSize generique '" << propId << "'. Code 0x" << hex << err << endl;
    return err;
}
EdsError Camera::setProp(const EdsPropertyID &propId, EdsVoid *propVal) const{
	EdsError err;
	EdsDataType dt;
	EdsUInt32 ds;

	err = EdsGetPropertySize(m_edsReference, propId, 0, &dt, &ds);
	if (!err){
		err = EdsSetPropertyData(m_edsReference, propId, 0, ds, propVal);
		Sleep(200);
		EdsGetEvent();
	}
	return err;
}
EdsError Camera::setPropByVal(const EdsPropertyID &propId, const EdsUInt32 &propVal) const{
	EdsError err;
	EdsUInt32 d = propVal;

	err = EdsSetPropertyData(m_edsReference, propId, 0, sizeof(EdsUInt32), &d);
	Sleep(200);
	EdsGetEvent();

	return err;
}

string Camera::getPropString(const EdsPropertyID &propID) const{
    EdsError err;
    EdsDataType dt;
    EdsUInt32 ds;
    EdsChar* propVal;
    string res;

    err = EdsGetPropertySize(m_edsReference, propID, 0, &dt, &ds);
    if (!err){
        propVal  = (EdsChar*)malloc(ds);
        err = EdsGetPropertyData(m_edsReference, propID, 0, ds, propVal);
        if (!err){
            res = string(propVal);
            free(propVal);
            return res;
        }
        free(propVal);
    }
    cout << "ERREUR get propSize generique str : 0x" << hex << err << endl;
    return string("ERREUR");
}

EdsError Camera::setSaveLocation(const EdsUInt32 &newSaveLocation){
	EdsError err = EDS_ERR_OK;
	EdsUInt32 saveLocation;

	if ((err = EdsGetPropertyData(m_edsReference, kEdsPropID_SaveTo, 0, sizeof(EdsUInt32), &saveLocation))){
		cout << "ERROR @Camera::setSaveOntoSD - cannot get current save location : 0x" << hex << err << endl;
		return err;
	}
	if (saveLocation != newSaveLocation){
		//Envoie propri�t� de savegarde sur le PC
		saveLocation = newSaveLocation;
		err = EdsSetPropertyData(m_edsReference, kEdsPropID_SaveTo, 0, sizeof(EdsUInt32), &saveLocation);
		if (err != EDS_ERR_OK)
			cerr << "ERROR @Camera::setSaveOntoSD : cannot set new save location : 0x" << err << endl;
		//Nouvelle capacit� m�moire
		EdsCapacity newCapacity = { 0x7FFFFFFF, 0x1000, 1 };
		err = EdsSetCapacity(m_edsReference, newCapacity);
	}
	return err;
}
EdsCameraRef Camera::getEdsReference() const{
    return m_edsReference;
}

unsigned int Camera::getId() const{
    return m_id;
}

string Camera::getName() const{
    return m_name;
}

ostream& operator<<(ostream &o, const Camera &cm){
    o << "Camera [" << cm.getId() << "] : " << cm.getName() << endl;
    return o;
}

EdsError Camera::setPictureSize(const string &size){ // CIMER CANON POUR LES OPERATIONS BINAIRES.
	EdsError err = EDS_ERR_OK;
	if (s_pictureSizes.find(size) == s_pictureSizes.end()){
		cout << "ERROR @Camera::setPictureSize : Cannot find picture size " << size << " !" << endl;
		return -1;
	}
	EdsUInt32 s;
	EdsGetPropertyData(m_edsReference, kEdsPropID_ImageQuality, 0, sizeof(EdsUInt32), &s);
	// d'abord remettre � z�ro les bits 24 � 31 dans s
	// puis on ajoute notre valeur
	s &= (0x00ffffff);
	s |= (s_pictureSizes[size] << 24);
	if ((err = EdsSetPropertyData(m_edsReference, kEdsPropID_ImageQuality, 0, sizeof(EdsUInt32), &s)))
		cout << "Cannot set picture size ! err : 0x" << hex << err << endl << " Value not supported by " << m_name << endl;
	return err;
}

// Les fonctions du Bulb ne font rien, mis � par retourner un code d'erreur inconnu...
EdsError Camera::startBulb(){
	EdsError err;
	if ( (err = EdsSendCommand(m_edsReference, kEdsCameraCommand_BulbStart, 0)) )
		m_bulb = true;
	return err;
}

EdsError Camera::endBulb(){
	EdsError err;
	if ((err = EdsSendCommand(m_edsReference, kEdsCameraCommand_BulbEnd, 0)))
		m_bulb = false;
	// TODO: S'assurer qu'on a l'objectEventHandler qui s'occupera de l'�v�nement de cr�ation de la photo pr�te � t�l�charger.
	return err;
}

EdsError Camera::pushTask(Task *t){
	m_tasks.push(t);
	return EDS_ERR_OK;
}

EdsError Camera::popTask(){
	if (m_tasks.size() >= 1){
		m_tasks.pop();
		return EDS_ERR_OK;
	}
	cout << "WARNING @Camera::popTask : taskQueue already empty !" << endl;
	return -1;
}

EdsError focusConfirmation(EdsStateEvent event, EdsUInt32 param, EdsVoid* context){
	cout << "focus confirmed ?" << endl;
	if (event == kEdsStateEvent_AfResult){
		EdsError err = EDS_ERR_OK;
		Camera* cam = (Camera*)context;
		cout << "focus done." << endl;
		err = EdsSendCommand(cam->getEdsReference(), kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_OFF);
		cam->popStateEventHandler();
		return err;
	}else
		return EDSW_EVENT_NOT_CAUGHT;
}

EdsError Camera::focus(){
	EdsError err = EDS_ERR_OK;
	if (m_liveView){
		err = EdsSendCommand(m_edsReference, kEdsCameraCommand_DoEvfAf, kEdsCameraCommand_EvfAf_OFF);
		cvWaitKey(60);
		err = EdsSendCommand(m_edsReference, kEdsCameraCommand_DoEvfAf, kEdsCameraCommand_EvfAf_ON);
	}
	EdsGetEvent(); // aucun �v�nemtn n'est d�clanch� lorsque le point est fait... Je laisse le code de focusConfirmation et du pushPropHandler au cas o� on trouverait une solution.
	//pushPropertyEventHandler(focusConfirmation);
	//pushStateEventHandler(focusConfirmation);
	EdsGetEvent();
	return err;
}

LiveViewDataSet* Camera::getLvDataSet(){
    return &m_lvDataSet;
}

EdsError Camera::takePicture(const string &filename, const bool &forceAF){
	EdsError err = EDS_ERR_OK;
	if (filename != "")
		m_pictureFilename = filename;
	if (forceAF){
		if ((err = EdsSendCommand(m_edsReference, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_Completely))){
			cout << "ERROR @Camera::takePicture - cannot press shutter button completely : 0x" << hex << err << endl;
			return err;
		}
		if ((err = EdsSendCommand(m_edsReference, kEdsCameraCommand_PressShutterButton, kEdsCameraCommand_ShutterButton_OFF))){
			cout << "ERROR @Camera::takePicture - cannot release shutter button : 0x" << hex << err << " - AF must have failed." << endl;
			return err;
		}
	}
	else
		if ((err = EdsSendCommand(m_edsReference, kEdsCameraCommand_TakePicture, 0))){
			cout << "ERROR @Camera::takePicture - kEdsCameraCommand_TakePicture failed: 0x" << hex << err << endl;
			return err;
		}
	EdsGetEvent();
	m_pictureDownloading = true;
	return err;
}

void Camera::setPictureFilename(const string &filename){
	m_pictureFilename = filename;
}

string Camera::getPictureFilename() const{
	return m_pictureFilename;
}
bool Camera::isPictureDownloading() const{
	return m_pictureDownloading;
}

EdsError Camera::downloadImage(const EdsDirectoryItemRef &directoryItem)
{
	cout << "OK @Camera::downloadImage : Begning file transfer" << endl;
	cout << "DBG: @Camera::downloadImage - ref = 0x" << hex << directoryItem << endl;
	EdsError err = EDS_ERR_OK;
	EdsStreamRef stream = NULL;
	EdsDirectoryItemInfo dirItemInfo;
	string filename = "";

	if ((err = EdsGetDirectoryItemInfo(directoryItem, &dirItemInfo))){
		cerr << "downloadImage Error Directory Item Info : 0x" << hex << err << endl;
		goto setPicDlFalseExit;
	}

	if (m_pictureFilename.length() > 0)
		filename = m_pictureFilename;
	else
		filename = dirItemInfo.szFileName;

	cout << "sz : " << dirItemInfo.szFileName << endl;
	cout << "m_filename : " << m_pictureFilename << endl;
	cout << "filename : " << filename << endl;

	err = EdsCreateFileStream(filename.c_str(), kEdsFileCreateDisposition_CreateAlways, kEdsAccess_ReadWrite, &stream);
	if (err != EDS_ERR_OK){
		cerr << "downloadImage Errorr Creating file stream : 0x" << hex << err << endl;
		goto setPicDlFalseExit;
	}

	if ((err = EdsDownload(directoryItem, dirItemInfo.size, stream))){
		cerr << "downloadImage Error DownLoad File : 0x" << hex << err << endl;
		goto setPicDlFalseExit;
	}
	EdsGetEvent();
	err = EdsDownloadComplete(directoryItem);
	if (err != EDS_ERR_OK){
		cerr << "downloadImage Error download complete : 0x" << hex << err << endl;
		goto setPicDlFalseExit;
	}

	if (stream != NULL){
		EdsRelease(stream);
		stream = NULL;
	}
setPicDlFalseExit:
	m_pictureDownloading = false;
	return err;
}

EdsError Camera::pushStateEventHandler(const EdswStateEventHandler& func){
	m_stateEventHandlers.push(func);
	Camera::stateEventHandler = &(m_stateEventHandlers.top());
	return EDS_ERR_OK;
}
EdsError Camera::pushObjectEventHandler(const EdswObjectEventHandler& func){
	m_objectEventHandlers.push(func);
	Camera::objectEventHandler = &(m_objectEventHandlers.top());
	return EDS_ERR_OK;
}
EdsError Camera::pushPropertyEventHandler(const EdswPropertyEventHandler& func){
    m_propertyEventHandlers.push(func);
    Camera::propertyEventHandler = &(m_propertyEventHandlers.top());
    return EDS_ERR_OK;
}
EdsError Camera::popStateEventHandler(){
	if (m_stateEventHandlers.size() > 1){
		m_stateEventHandlers.pop();
		Camera::stateEventHandler = &(m_stateEventHandlers.top());
		return EDS_ERR_OK;
	}
	cout << "ERROR: popStateEventHandler" << endl;
	return -1;
}
EdsError Camera::popObjectEventHandler(){
	if (m_objectEventHandlers.size() > 1){
		m_objectEventHandlers.pop();
		Camera::objectEventHandler = &(m_objectEventHandlers.top());
		return EDS_ERR_OK;
	}
	cout << "ERROR: popObjectEventHandler" << endl;
	return -1;
}
EdsError Camera::popPropertyEventHandler(){
    if (m_propertyEventHandlers.size() > 1){
        m_propertyEventHandlers.pop();
        Camera::propertyEventHandler = &(m_propertyEventHandlers.top());
        return EDS_ERR_OK;
    }
    cout << "ERROR: popPropertyEventHandler" << endl;
    return EDS_ERR_INTERNAL_ERROR;
}
CameraManager* Camera::getCameraManager(){
	return m_cm;
}
EdsBool Camera::isLiveViewOn() const{
    return m_liveView;
}
void Camera::setLiveView(const bool &lv){
	m_liveView = lv;
}
EdsError liveViewOnConfirmation(EdsPropertyEvent a, EdsPropertyID propID, EdsUInt32 propVal, EdsVoid* context){
    cout << "LiveView On confirmation - propID : 0x" << hex << propID << endl;
    if (propID == kEdsPropID_Evf_OutputDevice){
        cout << "LIVE VIEW ON CONFIRMATION" << endl;
        Camera* cam = (Camera*)context;
        cam->setLiveView(true);
		cout << "CONFIRMATION RECUE" << endl;
        cam->popPropertyEventHandler();
		EdsUInt32 DOF = kEdsEvfDepthOfFieldPreview_ON;
		return EdsSetPropertyData(cam->getEdsReference(), kEdsPropID_Evf_DepthOfFieldPreview, 0, sizeof(EdsUInt32), &DOF);
    }else{
        // l'�l�ment en dessous dans la pile traite l'appel
        return EDSW_EVENT_NOT_CAUGHT;
    }
}
EdsError liveViewOffConfirmation(EdsPropertyEvent a, EdsPropertyID propID, EdsUInt32 propVal, EdsVoid* context){
    cout << "LiveView Off confirmation - propID : 0x" << hex << propID << endl;
    if (propID == kEdsPropID_Evf_OutputDevice){
        cout << "LIVE VIEW OFF CONFIRMATION" << endl;
        Camera* cam = (Camera*)context;
        cam->setLiveView(false);
        cam->popPropertyEventHandler();

        EdsUInt32 DOF = kEdsEvfDepthOfFieldPreview_ON;
        return EdsSetPropertyData(cam->getEdsReference(), kEdsPropID_Evf_DepthOfFieldPreview, 0, sizeof(EdsUInt32), &DOF);
    }else{
        // l'�l�ment en dessous dans la pile traite l'appel
		return EDSW_EVENT_NOT_CAUGHT;
    }
}

EdsError Camera::startLiveView(){
    EdsError err = EDS_ERR_OK;
    EdsUInt32 device;
    if (!(err = EdsGetPropertyData(m_edsReference, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device))){
		device |= kEdsEvfOutputDevice_PC;
		cout << "Liveview output device : " << (int)device << endl;
        err = EdsSetPropertyData(m_edsReference, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);
		pushPropertyEventHandler(std::function<EdsError(EdsPropertyEvent, EdsPropertyID, EdsUInt32, EdsVoid*)>(liveViewOnConfirmation));
		cout << "attente liveview" << endl;
		while (!m_liveView){
			EdsGetEvent();
			cout << ".";
		}
    }
    return err;
}

EdsError Camera::endLiveView(){
    EdsError err = EDS_ERR_OK;
    EdsUInt32 device;
    if (!(err = EdsGetPropertyData(m_edsReference, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device))){
        device &= ~kEdsEvfOutputDevice_PC;
        err = EdsSetPropertyData(m_edsReference, kEdsPropID_Evf_OutputDevice, 0, sizeof(device), &device);
        EdsGetEvent();
        pushPropertyEventHandler(std::function<EdsError(EdsPropertyEvent, EdsPropertyID, EdsUInt32, EdsVoid*)>(liveViewOffConfirmation));
    }
    if (m_lvDataSet.evfRef != NULL) {
        EdsRelease(m_lvDataSet.evfRef);
        m_lvDataSet.evfRef = NULL;
    }

    return err;
}

// ne devrait pas �tre dans Camera::
void Camera::copyStream(Camera *cam, Task *t, EdsStreamRef from, EdsUInt32 size, EdsStreamRef to){
    t->m_error = EdsCopyData( from, size, to);
    t->m_status = t->m_error != EDS_ERR_OK ? FINISHED_FAILURE : FINISHED_SUCCESS;
}

EdsError Camera::allocLiveViewFrame(){
	EdsImageRef image2 = NULL;
	EdsError err = EDS_ERR_OK;

    if ((err = EdsCreateImageRef(m_lvDataSet.stream, &image2))){
		cerr << "ERROR @Camera::allocLiveViewFrame : EdsCreateImageRef: 0x" << hex << err << endl;
		return -1;
	}

	if ((err = EdsGetImageInfo(image2, kEdsImageSrc_FullView, &m_lvDataSet.imageInfo))){
		cerr << "ERROR @Camera::allocLiveViewFrame : EdsGetImageInfo: 0x" << hex << err << endl;
		return -1;
	}

    if (m_lvDataSet.imageInfo.componentDepth != 8){
		cerr << "ERROR @Camera::allocLiveViewFrame : imageInfo.componentDepth != 8" << endl;
		return -1;
	}

    m_lvDataSet.IplFrame = cvCreateImage(cvSize(m_lvDataSet.imageInfo.width, m_lvDataSet.imageInfo.height), IPL_DEPTH_8U, m_lvDataSet.imageInfo.numOfComponents);
    if (m_lvDataSet.IplFrame == NULL){
		cout << "ERROR @Camera::allocLiveViewFrame : Could not allocate the required memory for the liveview frame (m_lvDataSet.IplFrame = NULL)" << endl;
		return -1;
    }
    if (image2 != NULL)
        EdsRelease(image2);
	cout << "OK @Camera::allocLiveViewFrame : Properly allocated the required memory for the liveview frame" << endl;
	return err;
}

EdsError Camera::updateLiveView(const unsigned int &camID){
	CameraManager *cm = CameraManager::New();
	CameraPointer& cam = cm->getCamera(camID);
	if (cam.pointer == NULL){
		cout << "ERROR @Camera::updateLiveView : null pointer for camera (camID=" << camID << ")" << endl;
		return -1;
	}
	static bool isOfflinePictureOnScreen = false;
	if (!cam->isOnline()){
		if (!isOfflinePictureOnScreen){
            cam->m_lvDataSet.IplFrame = cvLoadImage("nocam2.jpg");
            cv::Mat c = cv::cvarrToMat(cam->m_lvDataSet.IplFrame);
			c.copyTo(cam->m_lvDataSet.frame);
			isOfflinePictureOnScreen = true;
		}
		return -1;
	}
	isOfflinePictureOnScreen = false;
	return cam->updateLiveView();
}

cv::Mat Camera::downloadLiveView(){
    static cv::Mat nocam4;
    static bool nocam4OnScreen = false;
    if (updateLiveView() != -1){
        nocam4OnScreen = false;
        return m_lvDataSet.frame;
    }
    if (!nocam4OnScreen){
        nocam4 = cv::cvarrToMat(cvLoadImage("nocam4.jpg"));
        nocam4OnScreen = true;
    }
    return nocam4;
}

cv::Mat Camera::downloadLiveView(const unsigned int &camID){
	static cv::Mat nocam3;
	static bool nocam3OnScreen = false;

	updateLiveView(camID);
	CameraPointer& cam = CameraManager::New()->getCamera(camID);
	if (cam.pointer == NULL){
		cout << "ERROR @Camera::downloadLiveView : null pointer for camera (camID=" << camID << ")" << endl;
		if (!nocam3OnScreen){
			nocam3OnScreen = true;
			nocam3 = cv::cvarrToMat(cvLoadImage("nocam3.jpg"));
		}
		return nocam3;
	}
	nocam3OnScreen = false;
	return cam->getlvFrame();
}

EdsError Camera::updateLiveView()
{
    time_t startTime = clock();
	static bool nocam1OnScreen = false;
	static bool nocam2OnScreen = false;
	static bool nocam3OnScreen = false;
	if (!m_liveView)
		return -1;

    LiveViewDataSet& lvDtSet = m_lvDataSet;

	EdsError err = EDS_ERR_OK;
    if (lvDtSet.stream == NULL){
        if ((err = EdsCreateMemoryStream(0, &lvDtSet.stream))) {
			cerr << "downloadLiveView Download Live View Image Error in Function EdsCreateMemoryStream: 0x" << hex << err << "\n";

			if (!nocam1OnScreen){
                lvDtSet.IplFrame = cvLoadImage("nocam1.jpg");
                cv::Mat c = cv::cvarrToMat(lvDtSet.IplFrame);
                c.copyTo(lvDtSet.frame);
			    return -1;
            }
		}
	}
    nocam1OnScreen = false;
    if (m_lvDataSet.evfRef == NULL)
	    if ((err = EdsCreateEvfImageRef(lvDtSet.stream, &m_lvDataSet.evfRef))) {
		    cerr << "downloadLiveView Download Live View Image Error in Function EdsCreateEvfImageRef: 0x" << hex << err << "\n";
		    if (!nocam2OnScreen){
			    lvDtSet.IplFrame = cvLoadImage("nocam2.jpg");
                cv::Mat c = cv::cvarrToMat(lvDtSet.IplFrame);
                c.copyTo(lvDtSet.frame);
			    nocam2OnScreen = true;
		    }
		    return -1;
	    }
	nocam2OnScreen = false;

	bool wait = true;
	int numTries = 0;
	while (wait && numTries++ < 20){
        err = EdsDownloadEvfImage(m_edsReference, m_lvDataSet.evfRef);
		if (err != EDS_ERR_OK && err != EDS_ERR_OBJECT_NOTREADY){
			cerr << "downloadLiveView Error in EdsDownloadEvfImage: 0x" << hex << err << endl;
            lvDtSet.IplFrame = cvLoadImage("nocam3.jpg");
            lvDtSet.frame = cv::cvarrToMat(lvDtSet.IplFrame);
			nocam3OnScreen = true;
			return -1;
		}
		if (err == EDS_ERR_OBJECT_NOTREADY){
			if (numTries <= 3) printf("downloadLiveView Waiting for camera\n");
			cvWaitKey(50);
		}
		else{
			if (numTries > 1) printf("stopped Waiting for camera\n");
			wait = false;
		}
	}
	nocam3OnScreen = false;

	EdsUInt32 size = 0;
	uchar *data = NULL;

    if (data == NULL)
	if ((err = EdsGetPointer(lvDtSet.stream, (EdsVoid**)& data))) {
		cerr << "downloadLiveView Download Live View Image Error in Function EdsGetPointer: 0x" << hex << err << "\n";
		return -1;
	}
    
    if (size == 0)
	    if ((err = EdsGetLength(lvDtSet.stream, &size))) {
		    cerr << "downloadLiveView Download Live View Image Error in Function EdsGetLength: 0x" << hex << err << "\n";
		    return -1;
	    }

	if (lvDtSet.IplFrame == NULL) 
		if (allocLiveViewFrame() == -1)
			return updateLiveView();
    
    tjhandle decompressor = tjInitDecompress();
    tjDecompress2(decompressor, (uchar*)data, size, (uchar*)lvDtSet.IplFrame->imageData, lvDtSet.IplFrame->width, lvDtSet.IplFrame->width * 3, lvDtSet.IplFrame->height, TJPF_BGR, NULL);
    tjDestroy(decompressor);

    lvDtSet.frame = cv::cvarrToMat(lvDtSet.IplFrame);
    //cout << "UpdateLiveView: " << (double)(clock()-startTime) / 1000 << "s" << endl;
	return err;
}

IplImage* Camera::getLvIplFrame(){
    return m_lvDataSet.IplFrame;
}

cv::Mat& Camera::getlvFrame(){
    return m_lvDataSet.frame;
}

void Camera::setLvFlip(const bool &flip){
	m_lvDataSet.flip = flip;
}

//!\\ Scroller davantage ne r�sultera qu'en tristesse et d�sepsoir. J'ai pr�venu.

/* *************************************************************************************** */
/* ******************   D E F I N I T I O N   D E S   S T A T I Q U E S   **************** */
/* *************************************************************************************** */

bool Camera::s_debug = false;

map<string, EdsUInt32> Camera::s_pictureSizes = {
	{ "L", 0 },
	{ "M", 1 },
	{ "S", 2 },
	{ "M1", 5 },
	{ "M2", 6 },
	{ "S1", 14 },
	{ "S2", 15 },
	{ "S3", 16 }
};

map<int, DisplayableValue<float>*> Camera::s_AV = {
    { 0x08, new DisplayableValue<float>(1,             "1") }, { 0x0B, new DisplayableValue<float>(1.1f,           "1.1") }, { 0x0C, new DisplayableValue<float>(1.2f,          "1.2") },
    { 0x0D, new DisplayableValue<float>(1.2f,  "1.2 (1/3)") }, { 0x10, new DisplayableValue<float>(1.4f,           "1.4") }, { 0x13, new DisplayableValue<float>(1.6f,          "1.6") },
    { 0x14, new DisplayableValue<float>(1.8f,        "1.8") }, { 0x15, new DisplayableValue<float>(1.8f,     "1.8 (1/3)") }, { 0x18, new DisplayableValue<float>(2,               "2") },
    { 0x1B, new DisplayableValue<float>(2.2f,        "2.2") }, { 0x1C, new DisplayableValue<float>(2.5f,           "2.5") }, { 0x1D, new DisplayableValue<float>(2.5f,    "2.5 (1/3)") },
    { 0x20, new DisplayableValue<float>(2.8f,        "2.8") }, { 0x23, new DisplayableValue<float>(3.2f,           "3.2") }, { 0x24, new DisplayableValue<float>(3.5f,          "3.5") },
    { 0x25, new DisplayableValue<float>(3.5f,  "3.5 (1/3)") }, { 0x28, new DisplayableValue<float>(4,                "4") }, { 0x2B, new DisplayableValue<float>(4.5f,          "4.5") },
    { 0x2C, new DisplayableValue<float>(4.5f,  "4.5 (1/3)") }, { 0x2D, new DisplayableValue<float>(5,                "5") }, { 0x30, new DisplayableValue<float>(5.6f,          "5.6") },
    { 0x33, new DisplayableValue<float>(6.3f,        "6.3") }, { 0x34, new DisplayableValue<float>(6.7f,           "6.7") }, { 0x35, new DisplayableValue<float>(7.1f,          "7.1") },
    { 0x38, new DisplayableValue<float>(8,             "8") }, { 0x3B, new DisplayableValue<float>( 9,               "9") }, { 0x3C, new DisplayableValue<float>(9.5f,          "9.5") },
    { 0x3D, new DisplayableValue<float>(10,           "10") }, { 0x40, new DisplayableValue<float>(11,              "11") }, { 0x43, new DisplayableValue<float>(13,       "13 (1/3)") },
    { 0x44, new DisplayableValue<float>(13,           "13") }, { 0x45, new DisplayableValue<float>(14,              "14") }, { 0x48, new DisplayableValue<float>(16,             "16") },
    { 0x4B, new DisplayableValue<float>(18,           "18") }, { 0x4C, new DisplayableValue<float>(19,              "19") }, { 0x4D, new DisplayableValue<float>(20,             "20") },
    { 0x50, new DisplayableValue<float>(22,           "22") }, { 0x53, new DisplayableValue<float>(25,              "25") }, { 0x54, new DisplayableValue<float>(27,             "27") },
    { 0x55, new DisplayableValue<float>(29,           "29") }, { 0x58, new DisplayableValue<float>(32,              "32") }, { 0x5B, new DisplayableValue<float>(36,             "36") },
    { 0x5C, new DisplayableValue<float>(38,           "38") }, { 0x5D, new DisplayableValue<float>(40,              "40") }, { 0x60, new DisplayableValue<float>(45,             "45") },
    { 0x63, new DisplayableValue<float>(51,           "51") }, { 0x64, new DisplayableValue<float>(54,              "54") }, { 0x65, new DisplayableValue<float>(57,             "57") },
    { 0x68, new DisplayableValue<float>(64,           "64") }, { 0x6B, new DisplayableValue<float>(72,              "72") }, { 0x6C, new DisplayableValue<float>(76,             "76") },
    { 0x6D, new DisplayableValue<float>(80,           "80") }, { 0x70, new DisplayableValue<float>(91,              "91") }, { 0x00, new DisplayableValue<float>(0,            "Auto") },
    { 0xffffff, new DisplayableValue<float>(0, "INCORRECT") }
};

map<int, DisplayableValue<int>*> Camera::s_ISO = {
    { 0x28, new DisplayableValue<int>(6,               "6") }, { 0x30, new DisplayableValue<int>(12,                "12") },
    { 0x38, new DisplayableValue<int>(25,             "25") }, { 0x40, new DisplayableValue<int>(50,                "50") },
    { 0x48, new DisplayableValue<int>(100,           "100") }, { 0x4B, new DisplayableValue<int>(125,              "125") },
    { 0x4D, new DisplayableValue<int>(160,           "160") }, { 0x50, new DisplayableValue<int>(200,              "200") },
    { 0x53, new DisplayableValue<int>(250,           "250") }, { 0x55, new DisplayableValue<int>(320,              "320") },
    { 0x58, new DisplayableValue<int>(400,           "400") }, { 0x5B, new DisplayableValue<int>(500,              "500") },
    { 0x5d, new DisplayableValue<int>(640,           "640") }, { 0x60, new DisplayableValue<int>(800,              "800") },
    { 0x63, new DisplayableValue<int>(1000,        "1 000") }, { 0x65, new DisplayableValue<int>(1250,           "1 250") },
    { 0x68, new DisplayableValue<int>(1600,        "1 600") }, { 0x70, new DisplayableValue<int>(3200,           "3 200") },
    { 0x78, new DisplayableValue<int>(6400,        "6 400") }, { 0x80, new DisplayableValue<int>(12800,         "12 800") },
    { 0x88, new DisplayableValue<int>(25600,      "25 600") }, { 0x90, new DisplayableValue<int>(51200,         "51 200") },
    { 0x98, new DisplayableValue<int>(102400,    "102 400") }, { 0x00, new DisplayableValue<int>(0,               "Auto") },
    { 0xffffff, new DisplayableValue<int>(0,   "INCORRECT") }
};

map<int, DisplayableValue<float>*> Camera::s_TV = {
    { 0x0C, new DisplayableValue<float>(0,          "Bulb") }, { 0x10, new DisplayableValue<float>(30,              "30") }, { 0x13, new DisplayableValue<float>(25,             "25") },
    { 0x14, new DisplayableValue<float>(20,           "20") }, { 0x15, new DisplayableValue<float>(20,        "20 (1/3)") }, { 0x18, new DisplayableValue<float>(15,             "15") },
    { 0x1B, new DisplayableValue<float>(13,           "13") }, { 0x1C, new DisplayableValue<float>(10,              "10") }, { 0x1D, new DisplayableValue<float>(10,       "10 (1/3)") },
    { 0x20, new DisplayableValue<float>(8,             "8") }, { 0x23, new DisplayableValue<float>(6,          "6 (1/3)") }, { 0x24, new DisplayableValue<float>(6,               "6") },
    { 0x25, new DisplayableValue<float>(5,             "5") }, { 0x28, new DisplayableValue<float>(4,                "4") }, { 0x2B, new DisplayableValue<float>(3.2f,          "3,2") },
    { 0x2C, new DisplayableValue<float>(3,             "3") }, { 0x2D, new DisplayableValue<float>(2.5f,           "2,5") }, { 0x30, new DisplayableValue<float>(2,               "2") },
    { 0x33, new DisplayableValue<float>(1.6f,       "1\"6") }, { 0x34, new DisplayableValue<float>(1.5f,          "1\"5") }, { 0x35, new DisplayableValue<float>(1.3f,         "1\"3") },
    { 0x38, new DisplayableValue<float>(1,             "1") }, { 0x3B, new DisplayableValue<float>(0.8f,          "0\"8") }, { 0x3C, new DisplayableValue<float>(0.7f,         "0\"7") },
    { 0x3D, new DisplayableValue<float>(0.6f,       "0\"6") }, { 0x40, new DisplayableValue<float>(0.5f,          "0\"5") }, { 0x43, new DisplayableValue<float>(0,           "0\"44") },
    { 0x44, new DisplayableValue<float>(0.3f,       "0\"3") }, { 0x45, new DisplayableValue<float>(0.3f,    "0\"3 (1/3)") }, { 0x48, new DisplayableValue<float>(1 / 4,         "1/4") },
    { 0x4B, new DisplayableValue<float>(1 / 5,       "1/5") }, { 0x4C, new DisplayableValue<float>(1 / 6,          "1/6") }, { 0x4D, new DisplayableValue<float>(1 / 6,   "1/6 (1/3)") },
    { 0x50, new DisplayableValue<float>(1 / 8,       "1/8") }, { 0x53, new DisplayableValue<float>(1 / 10,  "1/10 (1/3)") }, { 0x55, new DisplayableValue<float>(1 / 13,       "1/13") },
    { 0x58, new DisplayableValue<float>(1 / 15,     "1/15") }, { 0x5B, new DisplayableValue<float>(1 / 20,  "1/20 (1/3)") }, { 0x5C, new DisplayableValue<float>(1 / 20,       "1/20") },
    { 0x5D, new DisplayableValue<float>(1 / 25,     "1/25") }, { 0x60, new DisplayableValue<float>(1 / 30,        "1/30") }, { 0x63, new DisplayableValue<float>(1 / 40,       "1/40") },
    { 0x64, new DisplayableValue<float>(1 / 45,     "1/45") }, { 0x65, new DisplayableValue<float>(1 / 50,        "1/50") }, { 0x68, new DisplayableValue<float>(1 / 60,       "1/60") },
    { 0x6B, new DisplayableValue<float>(1 / 80,     "1/80") }, { 0x6C, new DisplayableValue<float>(1 / 90,        "1/90") }, { 0x6D, new DisplayableValue<float>(1 / 100,     "1/100") },
    { 0x70, new DisplayableValue<float>(1 / 125,   "1/125") }, { 0x73, new DisplayableValue<float>(1 / 160,      "1/160") }, { 0x74, new DisplayableValue<float>(1 / 180,     "1/180") },
    { 0x75, new DisplayableValue<float>(1 / 200,   "1/200") }, { 0x78, new DisplayableValue<float>(1 / 250,      "1/250") }, { 0x7B, new DisplayableValue<float>(1 / 320,     "1/320") },
    { 0x7C, new DisplayableValue<float>(1 / 350,   "1/350") }, { 0x7D, new DisplayableValue<float>(1 / 400,      "1/400") }, { 0x80, new DisplayableValue<float>(1 / 500,     "1/500") },
    { 0x83, new DisplayableValue<float>(1 / 640,   "1/640") }, { 0x84, new DisplayableValue<float>(1 / 750,      "1/750") }, { 0x85, new DisplayableValue<float>(1 / 800,     "1/800") },
    { 0x88, new DisplayableValue<float>(1 / 1000, "1/1000") }, { 0x8B, new DisplayableValue<float>(1 / 1250,    "1/1250") }, { 0x8C, new DisplayableValue<float>(1 / 1500,   "1/1500") },
    { 0x8D, new DisplayableValue<float>(1 / 1600, "1/1600") }, { 0x90, new DisplayableValue<float>(1 / 2000,    "1/2000") }, { 0x93, new DisplayableValue<float>(1 / 2500,   "1/2500") },
    { 0x94, new DisplayableValue<float>(1 / 3000, "1/3000") }, { 0x95, new DisplayableValue<float>(1 / 3200,    "1/3200") }, { 0x98, new DisplayableValue<float>(1 / 4000,   "1/4000") },
    { 0x9B, new DisplayableValue<float>(1 / 5000, "1/5000") }, { 0x9C, new DisplayableValue<float>(1 / 6000,    "1/6000") }, { 0x9D, new DisplayableValue<float>(1 / 6400,   "1/6400") },
    { 0xA0, new DisplayableValue<float>(1 / 8000, "1/8000") }, { 0xffffff, new DisplayableValue<float>(-1,       "ERROR") }, { 0x00, new DisplayableValue<float>(0,            "Auto") }
};

map<int, DisplayableValue<int>*> Camera::s_WB = {
    {  0, new DisplayableValue<int>(0,              "Auto") }, {  1, new DisplayableValue<int>(1,             "Daylight") }, {  2, new DisplayableValue<int>(2,              "Cloudy") },
    {  3, new DisplayableValue<int>(3,          "Tungsten") }, {  4, new DisplayableValue<int>(4,          "Fluorescent") }, {  5, new DisplayableValue<int>(5,               "Flash") },
    {  6, new DisplayableValue<int>(6,            "Manual") }, {  8, new DisplayableValue<int>(7,                "Shade") }, {  9, new DisplayableValue<int>(9,   "Color temperature") },
    { 10, new DisplayableValue<int>(10,             "PC-1") }, { 11, new DisplayableValue<int>(11,                "PC-2") }, { 12, new DisplayableValue<int>(12,               "PC-3") },
    { 15, new DisplayableValue<int>(15,         "Manual 2") }, { 16, new DisplayableValue<int>(16,            "Manual 3") }, { 18, new DisplayableValue<int>(18,           "Manual 4") },
    { 19, new DisplayableValue<int>(19,         "Manual 5") }, { 20, new DisplayableValue<int>(20,                "PC-4") }, { 21, new DisplayableValue<int>(21,               "PC-5") },
    { -1, new DisplayableValue<int>(-1,      "COORDINATES") }, { -2, new DisplayableValue<int>(-2,            "FROM PIC") }
};

map<int, DisplayableValue<int>*> Camera::s_AE = {
    {  0, new DisplayableValue<int>( 0,             "P") },  {  1, new DisplayableValue<int>( 1,                    "Tv") },  {  2, new DisplayableValue<int>( 2,                "Av") },
    {  3, new DisplayableValue<int>( 3,             "M") },  {  4, new DisplayableValue<int>( 4,                  "Bulb") },  {  5, new DisplayableValue<int>( 5,             "A-DEP") },
    {  6, new DisplayableValue<int>( 5,           "DEP") },  {  7, new DisplayableValue<int>( 6,   "settings registered") },  {  8, new DisplayableValue<int>( 7,              "Lock") },
    {  9, new DisplayableValue<int>( 9,        "Auto/+") },  { 10, new DisplayableValue<int>(10, "Night scene portraits") },  { 11, new DisplayableValue<int>(11,            "Sports") },
    { 12, new DisplayableValue<int>(12,      "Portrait") },  { 13, new DisplayableValue<int>(13,             "Landscape") },  { 14, new DisplayableValue<int>(14,          "Close-up") },
    { 15, new DisplayableValue<int>(15,     "Flash off") },  { 19, new DisplayableValue<int>(19,         "Creative auto") },  { 20, new DisplayableValue<int>(20,             "Movie") },
    { 21, new DisplayableValue<int>(21,"Photo in movie") },  { 0xffffff, new DisplayableValue<int>(-1,           "ERROR") }
};
map<int, DisplayableValue<int>*> Camera::s_meteringModes = {
    {  1, new DisplayableValue<int>( 1, "Spot metering") },  { 3, new DisplayableValue<int>(  3,   "Evaluative metering") },
    {  4, new DisplayableValue<int>( 4, "Partial metering") }, { 5, new DisplayableValue<int>(5, "Center-weighted averaging metering") },
    { 0xffffff, new DisplayableValue<int>(-1,   "ERROR") }
};
