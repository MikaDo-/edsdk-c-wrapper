#include "stdafx.h"
#include "DisplayableValue.h"
#include "Camera.h"
#include "CameraManager.h"
#include "EventManager.h"

using namespace EDSW;
using namespace std;

CameraManager* CameraManager::s_instance = NULL;
bool CameraManager::s_isInstantiated = false;

CameraManager::CameraManager(){
    EdsInitializeSDK();
    if (refreshCameraList()){
        cout << "ERROR : Can't obtain the list of the connected EOS devices." << endl;
        EdsTerminateSDK();
    }
}

CameraManager* CameraManager::New(){
	if (!s_isInstantiated){
		s_instance = new CameraManager();
		s_isInstantiated = true;
	}
	return s_instance;
}

bool CameraManager::isInstantiated(){
    return s_isInstantiated;
}

CameraManager::~CameraManager(){
	for each (Camera* cam in m_cameras)
		delete cam;
    EdsTerminateSDK();
	s_isInstantiated = false;
}

EdsError CameraManager::dumpCameraList(char *path){
	EdsCameraListRef camList;
	EdsUInt32 camCount = 0;
	EdsCameraRef cam;
	EdsError err;
	EdsInitializeSDK();
	_unlink(path);
	if (!(err = EdsGetCameraList(&camList))){
		EdsGetChildCount(camList, &camCount);
		if (camCount){
			FILE *listFile = NULL;
			fopen_s(&listFile, path, "a");
			for (unsigned int i(0); i<camCount; i++){
				cout << "Retrieving device informations [" << i << "]";
				if (!(err = EdsGetChildAtIndex(camList, i, &cam))){
					EdsDeviceInfo dev;
					EdsGetChildAtIndex(camList, i, &cam);
					if (!(err = EdsGetDeviceInfo(cam, &dev)))
						fprintf(listFile, "%s\n", dev.szDeviceDescription);
					else
						cout << "ERROR @CameraManager::dumpCameraList : Cannot get name for device " << i << endl;
				}else{
					cout << "\t\t\t [ERROR]" << endl;
					cout << "\t Can't retrieve informations for device [" << i << "] : code 0x" << hex << err << endl;
				}
			}
			fclose(listFile);
			EdsTerminateSDK();
			return 0;
		}else{
			cout << "No EOS camera detected !" << endl;
			EdsTerminateSDK();
			return -1;
		}
	}else{
		cout << "Impossible de recuperer la liste des appareils connectes !" << endl;
		EdsTerminateSDK();
		return 1;
	}
}

int CameraManager::refreshCameraList(){
    EdsCameraListRef camList;
    EdsUInt32 camCount = 0;
    EdsCameraRef cam;
    EdsError err;
    EventManager *em = EventManager::New(this);
    Camera *c = NULL;
	cout << "Rafraichissement de la liste des appareils connectes..." << endl;
	// Si on a des anciennes cameras dans la liste, on nettoie.
	for (vector<Camera*>::iterator it = m_cameras.begin(); it != m_cameras.end(); it++)
		delete *it;
	m_cameras.clear();
	for (vector<CameraPointer>::iterator it = m_cameraPointers.begin(); it != m_cameraPointers.end(); it++)
		it->pointer = NULL;
	// ---
    if (!(err = EdsGetCameraList(&camList))){
		EdsGetChildCount(camList, &camCount);
        if (camCount){
            for (unsigned int i(0); i<camCount; i++){
                cout << "Retrieving device informations [" << i << "]";
                if (!(err = EdsGetChildAtIndex(camList, i, &cam))){
                    cout << "\t\t\t [OK]" << endl;
                    c = new Camera(i, cam, this);
                    m_cameras.push_back(c);

					// On met � jour le pointeur vers la cam�ra si i < m_cameraPointers.size(), Sinon on en ajoute. Jamais on ne supprime (on met plut�t les pointeurs � NULL). Sinon, r�f�rence pendante c�t� utilisation !
					if (i < m_cameraPointers.size())
						m_cameraPointers[i].pointer = c;
					else
						m_cameraPointers.push_back(c);
					// ---

					em->setupCamera(c);
					c->openSession();
                    //c->askAvaibleSettings(); A faire un de ces jours... Pas vraiment vital
                }
                else{
                    cout << "\t\t\t [ERROR]" << endl;
                    cout << "\t Can't retrieve informations for device [" << i << "] : code 0x" << hex << err << endl;
                }
            }
            return 0;
        }
        else{
            cout << "No EOS camera detected !" << endl;
			return -1;
        }
    }
    else{
        cout << "Impossible de recuperer la liste des appareils connectes !" << endl;
        return 1;
    }
}

CameraPointer& CameraManager::getCamera(const unsigned int& index){
	if (index >= m_cameras.size()){
		cout << "ERROR @CameraManager::getCamera - wrong index (>camNumber)" << endl;
		return *(new CameraPointer());
	}
	return m_cameraPointers[index];
}

unsigned int CameraManager::getCameraCount() const{
	return m_cameras.size();
}

void CameraManager::openSessions(){
    EdsError err = EDS_ERR_OK;
    for (vector<Camera*>::iterator it = m_cameras.begin() ;it != m_cameras.end();it++){
        cout << "Session opening for DSLR [" << (*it)->getId() << "] : " << (*it)->getName();
        if( !(err =EdsOpenSession((*it)->getEdsReference())) )
            cout << "\t\t\t[OK]" << endl;
        else
            cout << "\t\t\t[ERROR]" << endl << "\t\t\t code 0x" << hex << err << endl;
    }
}

void CameraManager::closeSessions(){

}

Camera* CameraManager::operator[](const int &i){
    return m_cameras[i];
}

/*ostream& operator<<(ostream &o, CameraManager &cm){
    for (unsigned int i = 0 ;i < cm.getCameraCount(); i++)
        o << *(cm.getCamera(i).pointer) << endl;
    return o;
}*/
