#include "stdafx.h"
#include "DisplayableValue.h"
#include "Camera.h"
#include "CameraManager.h"
#include "EventManager.h"
#include "Task.h"
//#include "FileTransferTask.h"

using namespace std;
using namespace EDSW;

bool EventManager::s_isInstantiated = false;
EventManager* EventManager::s_instance = NULL;

EventManager* EventManager::New(CameraManager* cm){
    if (!s_isInstantiated)
        if (!cm){
            cout << "ERREUR: can't create a new instance of EventManager without specifying any CameraManager !" << endl;
            return NULL;
        }else
            s_instance = new EventManager(cm);
    return s_instance;
}

EventManager::EventManager(CameraManager* cm) : m_cm(cm)
{
    EdsSetCameraAddedHandler(EventManager::EdsCameraAddedHandler, cm);
}

void EventManager::setupCamera(Camera *cam){
    if (!cam){
        cout << "ERROR: EventManager::setupCamera NULL pointer for Camera parameter." << endl;
        return;
    }
    EdsCameraRef camID = cam->getEdsReference();

    EdsSetPropertyEventHandler(camID, kEdsPropertyEvent_All, EventManager::EdsPropertyEventHandler, cam);
    EdsSetCameraStateEventHandler(camID, kEdsStateEvent_All, EventManager::EdsStateEventHandler, cam);
	EdsSetObjectEventHandler(camID, kEdsObjectEvent_All, EventManager::EdsObjectEventHandler, cam);
}

EventManager::~EventManager()
{
}

EdsError EDSCALLBACK EventManager::EdsCameraAddedHandler(EdsVoid* context){
    CameraManager* cm = (CameraManager*)context;
	return cm->refreshCameraList();
}

//!\\ Les deux m�thodes ce-dessous contienent de la r�cursivit� ! Elles pourront donc mordre si elles se sentent menac�es.

// utiliser type de retour pour savoir si l'�v�nement � �t� pris en charge.
// s'il n'a pas �t� attrap�, refourguer l'event au handler suivant dans la pile : OH BIM LA BELLE RECURSIVIT�
// d�piler
// stocker r�sultat appel r�cursif
// repiler
// retourner resultat stock�

// Cette organisation est compromise si un handler par d�faut (tout en bas de la pile) retourne un EDSW_EVENT_NOT_CAUGHT.
// Ce qui aurait pour effet de nous faire d�piler ce handler par d�faut, et d'essayer d'acc�der � l'�l�ment en dessous (au premier �l�ment d'une pile vide).
// => CRASH

EdsError EDSCALLBACK EventManager::EdsObjectEventHandler(EdsObjectEvent event, EdsBaseRef ref, EdsVoid* context){
	EdsError res = EDS_ERR_OK;
    Camera* cam = (Camera*)context;
	cout << "EventManager : objectEvent" << endl;

	cout << cam->m_objectEventHandlers.size() << " object managers dans la pile" << endl;
	if ((res = (*(cam->objectEventHandler))(event, ref, context) == EDSW_EVENT_NOT_CAUGHT)){
		EdswObjectEventHandler f = *(cam->objectEventHandler);
		cam->popObjectEventHandler();
		res = EventManager::EdsObjectEventHandler(event, ref, context);
		cam->pushObjectEventHandler(f);
	}else
		cout << "OK @defaultObjectEventHandler : Object event successfuly caught !" << endl;
	return res;
}
EdsError EDSCALLBACK EventManager::EdsPropertyEventHandler(EdsPropertyEvent event, EdsPropertyID propID, EdsUInt32 param, EdsVoid* context){
	EdsError res = EDS_ERR_OK;
	Camera* cam = (Camera*)context;
	//cout << "EventManager : propertyEvent" << endl;
	//cout << cam->m_propertyEventHandlers.size() << " event managers dans la pile" << endl;
	if ((res = (*(cam->propertyEventHandler))(event, propID, param, context) == EDSW_EVENT_NOT_CAUGHT)){
		EdswPropertyEventHandler f = *(cam->propertyEventHandler);
		cam->popPropertyEventHandler();
		res = EventManager::EdsPropertyEventHandler(event, propID, param, context);
		cam->pushPropertyEventHandler(f);
	}
	//else{
		//cout << "OK @defaultPropertyEventHandler : Property event successfuly caught";
		//if (res)
			//cout << " but not properly processed";
		//cout << " and processed !" << endl;
	//}
	return res;
}
EdsError EDSCALLBACK EventManager::EdsStateEventHandler(EdsStateEvent event, EdsUInt32 eventData, EdsVoid* context){
	Camera* cam = (Camera*)context;
    cout << "-------------------------------------------------------------------------------------------------> EventManager : stateEvent" << endl;
	return (*(cam->stateEventHandler))(event, eventData, context);
}
