#include "stdafx.h"
#include "DisplayableValue.h"
#include "Camera.h"
#include "CameraManager.h"
#include "Task.h"
#include "TaskSequence.h"

using namespace EDSW;

TaskSequence::TaskSequence(Camera *cam, const TaskID &taskID, ...) : Task(cam), m_progress(0), m_thread(NULL)
{
}


TaskSequence::~TaskSequence()
{
}

void TaskSequence::pushTask(Task* t){
	m_tasks.push_back(std::pair<Task*, bool>(t, false));
}
bool TaskSequence::popTask(){
	if (m_tasks.size() >= 1){
		m_tasks.pop_back();
		return true;
	}
	return false;
}

unsigned int TaskSequence::size(){
	return m_tasks.size();
}
void TaskSequence::clear(){
	m_tasks.clear();
}
bool TaskSequence::start(){
	if (m_thread != NULL)
		return false;
	m_thread = new std::thread(&TaskSequence::run, this);
	return true;
}
void TaskSequence::run(){
	for (vector<pair<Task*, bool>>::iterator it = m_tasks.begin(); it != m_tasks.end(); it++){
		if (!m_canceled)
			break;
	}
}

unsigned int TaskSequence::getProgress() const{
	return m_progress;
}